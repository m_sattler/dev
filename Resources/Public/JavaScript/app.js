$(function() {
    $('.content-slider').slick({
        arrows: true,
        autoplay: true,
        autoplaySpeed: 6000,
    });
 
});

$(document).ready(function () {
    $('iframe').on('load', function() {
        $("iframe").contents().find(".button").css("display", "none");
    });
});
$(document).foundation()